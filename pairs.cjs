function pairs(obj) {
    if(typeof obj !== 'object' || !obj) {
        return [];
    }
    let result = [];
    for(const key in obj) {
        result.push([key, obj[key]]);
    }
    return result;
}

module.exports = pairs;