function values(obj) {
    if(typeof obj !== 'object' || !obj) {
        return [];
    }
    let valueArray = [];
    for(const key in obj) {
        if(typeof obj[key] !== 'function') {
            valueArray.push(obj[key]);
        }
    }
    return valueArray;
}

module.exports = values;