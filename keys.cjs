function keys(obj) {
    if(typeof obj !== 'object' || !obj) {
        return [];
    }
    let keyArray = [];
    for(const key in obj) {
        const strigyfiedKey = key.toString();
        keyArray.push(strigyfiedKey);
    }
    return keyArray;
}

module.exports = keys;