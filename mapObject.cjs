function mapObject(obj, cb) {
    if(!obj || typeof obj !== 'object' || !cb || typeof cb !== 'function') {
        return [];
    }
    let mappedObj = {};
    for(const key in obj) {
        if(typeof obj[key] === 'function') {
            mappedObj[key] = obj[key]();
        } else {
            mappedObj[key] = cb(obj[key]);
        }
    }
    return mappedObj;
}

module.exports = mapObject;