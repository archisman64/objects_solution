const mapObjects = require('../mapObject.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham', findLocation: function() {} };

function callback (value) {
    return value + 'transformed';
}
console.log(mapObjects(testObject, callback));