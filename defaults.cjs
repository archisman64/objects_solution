function defaults(obj, defaultProps) {
    if(!obj || typeof obj !== 'object'){
        return {};
    }
    if(!defaultProps || typeof defaultProps !== 'object') {
        return obj;
    }
    for(const key in defaultProps) {
        if(!(key in obj)){
            console.log('found non existant value:',key, obj[key]);
            obj[key] = defaultProps[key];

        } else {
            if(obj[key] !=0 && !obj[key]) {
                obj[key] = defaultProps[key];
            }
        }
    }
    return obj;
}

module.exports = defaults;